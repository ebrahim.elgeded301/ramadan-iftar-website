import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export const store = new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    loggedUserInfo: ""
  },
  mutations: {
    SET_LOGGED_USER(state, userInfo) {
      state.loggedUserInfo = userInfo;
    },
    SET_LOGOUT_USER(state) {
      state.loggedUserInfo = "";
    }
  },
  getters: {
    getUserInfo: state => {
      return state.loggedUserInfo;
    }
  }
});
